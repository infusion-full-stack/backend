const { Router } = require('express');
const { check } = require('express-validator');
const { getUsers, updateUser, createUser, deleteUser } = require('./../controllers/user')
const { validateInputs } = require('../../middlewares/validators')
const { validateIdUser } = require('../../helpers/validatorsDB')
const router = Router();

router.get('/', getUsers);

router.post('/', [
    check('password', 'debe tener más de 5 caracteres')
        .isLength({ min: 6 })
        .notEmpty()
        .withMessage('es requerido'),
    check('email', 'no tiene un formato valido').isEmail(),
    check('name', 'es requerido').notEmpty(),
    check('role', 'no es un rol valido').isIn(['Admin', 'User']),
    validateInputs
]
    , createUser);

router.put('/:id', [
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(validateIdUser),
    validateInputs
], updateUser);

router.delete('/:id',[check('id', 'No es un id valido').isMongoId(),
check('id').custom(validateIdUser),
validateInputs], deleteUser);

module.exports = router;
