const { Schema, model } = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

let validRoles = {
   values: ['Admin', 'User'],
   message: '{VALUE} no es un rol válido'
};
let validStatus = {
   values: ['Activo', 'Inactivo'],
   message: '{VALUE} no es un estatus válido'
};

let UserSchema = new Schema({
   name: { type: String, required: true },
   email: { type: String, unique: true, required: true },
   password: { type: String, required: true },
   role: { type: String, default: 'User', enum: validRoles },
   status: { type: String, default:'Activo', enum: validStatus },
   creationDate: { default: new Date(), type: Date }

})

UserSchema.plugin(uniqueValidator, { message: '{PATH} debe ser único' })

UserSchema.methods.toJSON = function(){
   const {__v, ...user } = this.toObject();
   return user;
}

module.exports = model('User', UserSchema);