const bcrypt = require('bcrypt');
const User = require('../models/user');
const saltRounds = 10;
const { response } = require('express');

const createUser = async (req, res) => {

    const { name, email, password, role, status } = req.body;

    let user = new User({
        name,
        email,
        password,
        role,
        status
    })

    const validateUniqueEmail = await User.findOne({ email });
    if (validateUniqueEmail) {
        return res.status(400).json({
            error: `El email ${email} ya existe`
        });
    }


    const salt = bcrypt.genSaltSync(saltRounds);
    user.password = bcrypt.hashSync(password, salt);
    user.save((err, userDB) => {
        if (err) {
            return res.status(400).json({
                err
            });

        } else {
            res.status(201).json({
                msg:'Usuario registrado',
                user: userDB,
            });
        }
    })
}


const getUsers = async (req, res = response) => {
    const { name, email, status } = req.query;
    let filterName = {}
    let filterEmail = {}
    let filterStatus = {}

    if (name) {
        filterName = { 'name': name };
    }

    if (email) {
        filterEmail = { 'email': email }
    }

    if (status) {
        filterStatus = { 'status': status };
    }

    const users = await User.find({ $and: [filterName, filterEmail, filterStatus] }).sort({creationDate:-1})

    // if(users.length==0){
    //     res.json({
    //         msg:"No hemos encontrado registros :(",
    //         users
    //     })

    // }

    res.json({
        users
    })

}


const updateUser = async (req, res) => {
    const { id } = req.params;
    const { _id, password, ...userUpdate } = req.body
    if (password) {
        const salt = bcrypt.genSaltSync(saltRounds);
        userUpdate.password = bcrypt.hashSync(password, salt);
    }
    const user = await User.findByIdAndUpdate(id, userUpdate);
    res.json({
        msg:'Registro actualizado',
        user
    })
}

const deleteUser = async (req, res) => {
    const { id } = req.params;

    const user = await User.findByIdAndDelete(id)

    res.json({
        msg:'Registro eliminado',
        user
    })
}


module.exports = {
    createUser, getUsers, updateUser, deleteUser
};