# Backend | CRUD de usuarios

ApiRest desarrollada con nodeJS y express, con conexion a base de datos mongoDB y uso del ODM Mongoose

Esta aplicacion  se conecta a un cluster de mongo DB atlas, este acceso se mantedra de forma temporal hasta realizar la revision correspondiente.

Por lo cual require internet para conectar a la base de datos

## Requerimientos

* Node v12.18.1
* Git

## Development server

Clona el proyecto `https://gitlab.com/infusion-full-stack/backend.git`

Ejecutar en el directorio principal de la aplicacion  `npm install`

Ejecuta `node index.js`  desde el directorio principal  para ejecutar la aplicacion en dev. Luego navega a `http://localhost:3000/`

Se incluye la dependenciade desarrollo `nodemon index.js`, por lo cual puede ejecutar 
esta comando para ejecutar la aplicacion

Esta aplicacion solo contiene un ambiente de dev

## Code scaffolding

Se agrupan los modulos de la aplicacion por carpetas, que contienen 
`routes` `controllers` `models` adicional a esto la carpetas base que poseen recursos globales.

Se busca mantener una `arquitectura hexagonal`

