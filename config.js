// ======================
// Puertos
// ======================

process.env.PORT = process.env.PORT || 3000

// ======================
// Entorno
// ======================

process.env.NODE_ENV = process.env.NODE_ENV || 'dev'


// ======================
// CORS 
// ======================
process.env.CORS ={
    origin: 'http://localhost:4200',
    optionsSuccessStatus: 200 
  }

// ======================
// Base de datos
// ======================
  process.env.MONGO_CONEXION_DEV = 'mongodb+srv://infusion-admin:Z7aIuQePOQVCQgaL@api-rest.wvcgx.mongodb.net/infusion_investments?retryWrites=true&w=majority'
