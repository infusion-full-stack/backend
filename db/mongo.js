const mongoose = require('mongoose');

const connectionMongoDB = async () => {
  try {

    await mongoose.connect(process.env.MONGO_CONEXION_DEV, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    });

    console.log('Conectado a MongoDB')

  } catch (error) {
    console.log(error);
    throw new Error('Error conectando a MongoDB');
  }

}

module.exports = {
  connectionMongoDB
}
