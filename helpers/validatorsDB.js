const User = require('../users/models/user');

const validateIdUser = async (id) => {
    const userExists = await User.findById(id);
    if (!userExists) {
        throw new Error(`El id ${id} no existe `);
    }
}

module.exports = { validateIdUser } 
